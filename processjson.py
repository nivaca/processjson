import os
import json
import sys
import re

default_ext = ".json"
new_ext = ".md"  # markdown
directory = "test"


def get_files():
    """ Returns the list of names of the xml files
    in the data directory. """

    fnames = os.listdir(directory)
    file_list = []

    for file in fnames:
        fname = os.path.splitext(file)[0]
        fext = os.path.splitext(file)[1]
        # check whether file has required extension
        if fext == default_ext:
            file_list.append([fname, fext])

    file_list = sorted(file_list)

    if len(file_list) == 0:
        print("Error: No files to process")
        exit(0)
    return file_list


def read_file(filename):
    fullname = directory + '/' + filename[0] + filename[1]
    with open(fullname) as f:
        content = f.read()  # read the full file
    return json.loads(content)  # parse the json


def clean_name(thetext):
    thetext = re.sub(r"\n", " ", thetext)
    thetext = re.sub(r"[\"\'\{\}\%\~]", " ", thetext)
    thetext = re.sub(r"[.,:;/\\]", " ", thetext)
    thetext = re.sub(r"\t{1,8}", " ", thetext)
    thetext = re.sub(r"[ ]{2,8}", " ", thetext)
    thetext = re.sub(r"[ ]{2,8}", " ", thetext)
    thetext = thetext.strip()
    return thetext


def process_file(filename):
    print(f"Processing {filename[0]+filename[1]}...", end='')
    parsed_json = read_file(filename)
    fullcontent = parsed_json['content']
    splitcontent = fullcontent.splitlines()
    if len(splitcontent) == 0:
        newname = clean_name(fullcontent)
        division = ""
        for i in range(len(fullcontent)):
            division += '='
        firstline = fullcontent + '\n' + division + '\n'
    else:
        newname = clean_name(splitcontent[0])
        division = ""
        for i in range(len(splitcontent[0])):
            division += '='
        firstline = splitcontent[0] + '\n' + division + '\n'

    tags = (parsed_json['tags'])
    if len(tags) == 0:
        subdir = 'Uncategorised'
    else:
        subdir = tags[0].strip().capitalize()
        subdir = clean_name(subdir)

    fullsubdir = directory + '/' + subdir
    if not os.path.exists(fullsubdir):
        try:
            os.makedirs(fullsubdir)
        except IOError:
            print(f"WARNING: Could not create subdir {fullsubdir}")

    fullnamedest = directory + '/' + subdir + '/' + newname + new_ext

    try:
        file = open(fullnamedest, 'w')
    except IOError:  # use original filename + new ext if not possible
        fullnamedest = directory + '/' + filename[0] + new_ext
        try:
            file = open(fullnamedest, 'w')
        except IOError:
            print(f"Could not create file {fullnamedest}")
            sys.exit(0)

    file.write(firstline)
    if len(splitcontent) != 0:
        for i in range(1, len(splitcontent)):
            file.write(splitcontent[i] + '\n')

    file.close()
    return True


def main():
    """ Main function. """
    file_list = get_files()
    for f in file_list:
        if process_file(f):
            filename = f[0]+f[1]
            print("OK!")


if __name__ == "__main__":
    main()